// // Copyright (c) Your Name
// // Distributed under the terms of the Modified BSD License.

import {
  DOMWidgetModel,
  DOMWidgetView,
  ISerializers,
} from '@jupyter-widgets/base';

import { MODULE_NAME, MODULE_VERSION } from './version';

// Import the CSS
import '../css/widget.css';

// export class EmailModel extends DOMWidgetModel {
//   defaults() {
//     return {
//       ...super.defaults(),
//       _model_name: EmailModel.model_name,
//       _model_module: EmailModel.model_module,
//       _model_module_version: EmailModel.model_module_version,
//       _view_name: EmailModel.view_name,
//       _view_module: EmailModel.view_module,
//       _view_module_version: EmailModel.view_module_version,
//       value: 'Hello World',
//     };
//   }

//   static serializers: ISerializers = {
//     ...DOMWidgetModel.serializers,
//     // Add any extra serializers here
//   };

//   static model_name = 'EmailModel';
//   static model_module = MODULE_NAME;
//   static model_module_version = MODULE_VERSION;
//   static view_name = 'EmailView'; // Set to null if no view
//   static view_module = MODULE_NAME; // Set to null if no view
//   static view_module_version = MODULE_VERSION;
// }

// export class EmailView extends DOMWidgetView {
//   render() {
//     // this.el.classList.add('custom-widget');

//     // this.value_changed();
//     // this.model.on('change:value', this.value_changed, this);
//     this._emailInput = document.createElement('input');
//     this._emailInput.type = 'email';
//     this._emailInput.value = 'example@example.com';
//     this._emailInput.disabled = true;
//     this.el.appendChild(this._emailInput);

//     this.el.classList.add('custom-widget');

//     this.value_changed();
//     this.model.on('change:value', this.value_changed, this);
//   }

//   value_changed() {
//     this.el.textContent = this.model.get('value');
//   }
// }

export class EmailModel extends DOMWidgetModel {
  defaults() {
    return {
      ...super.defaults(),
      _model_name: EmailModel.model_name,
      _model_module: EmailModel.model_module,
      _model_module_version: EmailModel.model_module_version,
      _view_name: EmailModel.view_name,
      _view_module: EmailModel.view_module,
      _view_module_version: EmailModel.view_module_version,
      value: 'Hello World',
    };
  }

  static serializers: ISerializers = {
    ...DOMWidgetModel.serializers,
    // Add any extra serializers here
  };

  static model_name = 'EmailModel';
  static model_module = MODULE_NAME;
  static model_module_version = MODULE_VERSION;
  static view_name = 'EmailView';
  static view_module = MODULE_NAME;
  static view_module_version = MODULE_VERSION;
}

export class EmailView extends DOMWidgetView {
  // private _emailInput: HTMLInputElement;

  render() {
    // this.el.classList.add('custom-widget');

    // this.value_changed();
    // this.model.on('change:value', this.value_changed, this);

    this._emailInput = document.createElement('input');
    this._emailInput.type = 'email';
    // this._emailInput.value = 'example@example.com';
    // this._emailInput.disabled = true;
    this._emailInput.value = this.model.get('value');
    this._emailInput.disabled = this.model.get('disabled');
    this.el.appendChild(this._emailInput);

    this.el.appendChild(document.createElement('br'));
    this._passwordInput = document.createElement('input');
    this._passwordInput.type = 'password';
    this.el.appendChild(this._passwordInput);

    this.el.appendChild(document.createElement('br'));
    this._button = document.createElement('button');
    this._button.innerHTML = 'Submit';
    this.el.appendChild(this._button);

    this._button.onclick = this._onSubmitClick.bind(this);

    this.el.classList.add('custom-widget');

    // this.value_changed();
    // this.model.on('change:value', this.value_changed, this);

    // Python -> JavaScript update
    this.model.on('change:value', this._onValueChanged, this);
    this.model.on('change:disabled', this._onDisabledChanged, this);

    // JavaScript -> Python update
    this._emailInput.onchange = this._onInputChanged.bind(this);
  }

  // value_changed() {
  //   this.el.textContent = this.model.get('value');
  // }

  private _onValueChanged() {
    this._emailInput.value = this.model.get('value');
  }

  private _onDisabledChanged() {
    this._emailInput.disabled = this.model.get('disabled');
  }

  private _onInputChanged() {
    this.model.set('value', this._emailInput.value);
    this.model.save_changes();
  }

  private async _onSubmitClick() {
    // const baseUrl = 'https://c5m14nm4m1.execute-api.us-east-1.amazonaws.com';
    const baseUrl = 'https://cwed398ql9.execute-api.us-east-1.amazonaws.com';
    const surveyStackBaseUrl = 'https://dev.surveystack.io';
    const resp = await fetch(`${surveyStackBaseUrl}/api/auth/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: this._emailInput.value,
        password: this._passwordInput.value,
      }),
    });
    const data = await resp.json();

    const authHeader = `${data.email} ${data.token}`;
    console.log(authHeader);
    const resp2 = await fetch(`${baseUrl}/fields`, {
      headers: { 'X-Authorization': authHeader },
      // mode: 'no-cors',
    });
    const data2 = await resp2.json();

    console.log(data, data2);
    console.log('login', this._emailInput.value, this._passwordInput.value);
  }

  private _emailInput: HTMLInputElement;
  private _passwordInput: HTMLInputElement;
  private _button: HTMLButtonElement;
}
