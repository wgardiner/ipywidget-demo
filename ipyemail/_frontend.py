#!/usr/bin/env python
# coding: utf-8

# Copyright (c) Your Name.
# Distributed under the terms of the Modified BSD License.

"""
Information about the frontend package of the widgets.
"""

module_name = "jupyter-email"
module_version = "^0.1.0"
